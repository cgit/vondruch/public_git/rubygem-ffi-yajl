# Generated from ffi-yajl-2.3.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name ffi-yajl

Name: rubygem-%{gem_name}
Version: 2.3.0
Release: 1%{?dist}
Summary: Ruby FFI wrapper around YAJL 2.x
# The license in .gempsec seems to be wrong.
# https://github.com/chef/ffi-yajl/pull/86
License: MIT
URL: http://github.com/chef/ffi-yajl
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# This provides libyajl2 gem stub to avoid the dependency.
Patch0: rubygem-ffi-yajl-2.3.0-Provide-libyajl2-stub-to-avoid-the-need-for-libyajl2.patch
# Use versioned .so file, otherwise yajl-devel would be needed.
# https://github.com/chef/ffi-yajl/pull/83
Patch1: rubygem-ffi-yajl-2.3.0-Use-versioned-.so-file.patch
Requires: yajl
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby-devel >= 2.1
# Compiler is required for build of gem binary extension.
# https://fedoraproject.org/wiki/Packaging:C_and_C++#BuildRequires_and_Requires
BuildRequires: gcc
BuildRequires: yajl-devel
BuildRequires: rubygem(rspec)

%description
Ruby FFI wrapper around YAJL 2.x.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%patch0 -p1

# Add fake libyajl2.rb introduced by patch0 into the file list.
sed  -i '/s.files/ s|\]$|, "lib/libyajl2.rb".freeze]|' %{gem_name}.gemspec
%gemspec_remove_dep -g libyajl2 "~> 1.2"

%patch1 -p1


%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
export RUBYOPT=-I$(pwd)/lib
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{gem_extdir_mri}/ffi_yajl/ext
cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
cp -a .%{gem_extdir_mri}/ffi_yajl/ext/*.so %{buildroot}%{gem_extdir_mri}/ffi_yajl/ext/

# Prevent dangling symlink in -debuginfo (rhbz#878863).
rm -rf %{buildroot}%{gem_instdir}/ext/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x


%check
pushd .%{gem_instdir}
rspec -I$(dirs +1)%{gem_extdir_mri} spec
popd

%files
%dir %{gem_instdir}
%{_bindir}/ffi-yajl-bench
%{gem_extdir_mri}
%license %{gem_instdir}/LICENSE
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_libdir}/ffi_yajl/ext/.keep
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/spec

%changelog
* Mon May 29 2017 Vít Ondruch <vondruch@redhat.com> - 2.3.0-1
- Initial package
